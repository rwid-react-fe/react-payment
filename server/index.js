require("dotenv").config();

const express = require("express");
const cors = require("cors");

const app = express();

app.use(cors());
app.use(
  express.urlencoded({
    extended: true,
  })
);
app.use(express.json());

const MIDTRANS_AUTH_KEY = Buffer.from(process.env.MIDTRANS_SERVER_KEY).toString(
  "base64"
);

const DEFAULT_MIDTRANS_REQUEST_HEADERS = {
  "Content-Type": "application/json",
  Authorization: `Basic ${MIDTRANS_AUTH_KEY}:`,
};

const getSnapToken = async (body) => {
  const response = await fetch(
    "https://app.sandbox.midtrans.com/snap/v1/transactions",
    {
      method: "POST",
      headers: DEFAULT_MIDTRANS_REQUEST_HEADERS,
      body,
    }
  );

  return await response.json();
};

app.post("/m/snap", async (req, res, next) => {
  const body = JSON.stringify(req.body);
  const json = await getSnapToken(body);

  res.status(201).json(json);
});

const getCardToken = async () => {
  const response = await fetch(
    `https://api.sandbox.midtrans.com/v2/token?client_key=${process.env.MIDTRANS_CLIENT_KEY}&card_number=4811111111111114&card_cvv=123&card_exp_month=12&card_exp_year=2025`,
    {
      method: "GET",
      headers: DEFAULT_MIDTRANS_REQUEST_HEADERS,
    }
  );

  return await response.json();
};

app.get("/m/token", async (req, res, next) => {
  const json = await getCardToken();

  res.status(201).json(json);
});

const chargeTransaction = async (body) => {
  const response = await fetch("https://api.sandbox.midtrans.com/v2/charge", {
    method: "POST",
    headers: DEFAULT_MIDTRANS_REQUEST_HEADERS,
    body,
  });

  return await response.json();
};

app.post("/m/charge", async (req, res, next) => {
  const body = JSON.stringify(req.body);
  const json = await chargeTransaction(body);

  res.status(201).json(json);
});

const XENDIT_AUTH_KEY = Buffer.from(
  `${process.env.XENDIT_SERVER_KEY}:`
).toString("base64");

const DEFAULT_XENDIT_REQUEST_HEADERS = {
  "Content-Type": "application/json",
  Authorization: `Basic ${XENDIT_AUTH_KEY}`,
};

const createInvoice = async (body) => {
  const response = await fetch("https://api.xendit.co/v2/invoices", {
    method: "POST",
    headers: DEFAULT_XENDIT_REQUEST_HEADERS,
    body,
  });

  return await response.json();
};

app.post("/x/invoices", async (req, res, next) => {
  const body = JSON.stringify(req.body);
  const json = await createInvoice(body);

  res.status(201).json(json);
});

const Stripe = require("stripe");
const stripe = Stripe(process.env.STRIPE_SERVER_KEY);

app.post("/s/sessions", async (req, res, next) => {
  const json = await stripe.checkout.sessions.create(req.body);

  res.status(201).json(json);
});

app.listen(3000, () => console.log("Server running..."));
