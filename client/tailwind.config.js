// tailwind.config.js
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        royalBlue: "#091235",
        navyBlue: "#14202E",
        midnightBlue: "#2B4257",
        blueGray: "#88A9C3",
      },
    },
  },
  plugins: [],
};
