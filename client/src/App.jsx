import { RouterProvider, createBrowserRouter } from "react-router-dom";

import Home from "./Pages/Home";
import MidtransCheckout from "./Pages/Checkout/Midtrans";

const router = createBrowserRouter([
  {
    path: "/",
    index: true,
    element: <Home />,
  },
  {
    path: "/checkout",
    children: [
      {
        path: "m",
        element: <MidtransCheckout />,
      },
    ],
  },
]);

const App = () => {
  return <RouterProvider router={router} />;
};

export default App;
