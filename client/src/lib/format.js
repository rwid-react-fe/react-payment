export const formatPrice = (price) => {
  return new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  }).format(price);
};

export const formatCartItem = (cart) =>{
  return cart.product.map(({ name, price, qty: quantity }) => ({
    name,
    price,
    quantity,
  }));
}