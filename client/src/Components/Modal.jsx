const Modal = ({ isOpen, onClose, children }) => {
  const overlayClasses = isOpen
    ? "fixed inset-0 bg-black bg-opacity-20 transition-opacity"
    : "hidden";
  const modalClasses = isOpen
    ? "fixed inset-0 flex items-center justify-center"
    : "hidden";

  return (
    <div className={overlayClasses}>
      <div className={modalClasses}>
        <div className="modal-overlay absolute inset-0 bg-gray-900 opacity-75"></div>

        <div className="modal-container bg-white w-11/12 md:max-w-md mx-auto rounded shadow-lg z-50 overflow-y-auto">
          <div
            className="modal-close absolute top-0 right-0 cursor-pointer flex flex-col items-center mt-4 mr-4 text-white text-sm z-50"
            onClick={onClose}
          >
            <svg
              className="fill-current text-white"
              xmlns="http://www.w3.org/2000/svg"
              width="18"
              height="18"
              viewBox="0 0 18 18"
            >
              <path d="M18 1.5l-1.5-1.5-7.5 7.5-7.5-7.5-1.5 1.5 7.5 7.5-7.5 7.5 1.5 1.5 7.5-7.5 7.5 7.5 1.5-1.5-7.5-7.5 7.5-7.5z" />
            </svg>
            <span className="text-sm">(Esc)</span>
          </div>

          <div className="modal-content py-4 text-left px-6">{children}</div>
        </div>
      </div>
    </div>
  );
};

export default Modal;
