import { nanoid } from "nanoid";
import { useDispatch } from "react-redux";

import { formatCartItem } from "../../../lib/format";
import { useCart, clear } from "../../../store/cart/cartSlice";

/**
 * @param {RequestInfo | URL} endpoint
 * @param {RequestInit} init
 * @returns
 */
const makeRequest = async (endpoint, init) => {
  const response = await fetch(`http://localhost:3000/x${endpoint}`, {
    headers: {
      "Content-Type": "application/json",
    },
    ...init,
  });

  return await response.json();
};

const generateRequestBody = (cart) => {
  const items = formatCartItem(cart);
  const id = nanoid(6);

  return {
    external_id: id,
    amount: cart.totalPrice,
    description: `Invoice Demo #${id}`,
    success_redirect_url: "https://www.google.com",
    failure_redirect_url: "https://www.google.com",
    currency: "IDR",
    items,
  };
};

const useXendit = () => {
  const dispatch = useDispatch();

  const cart = useCart();

  const isDisabled = !cart.totalQty;

  const onClick = async () => {
    try {
      const request = generateRequestBody(cart);

      const invoices = await makeRequest("/invoices", {
        method: "POST",
        body: JSON.stringify(request),
      });

      window.open(invoices.invoice_url, "_blank");

      dispatch(clear());

      return;
    } catch (err) {
      console.log(err);
      throw err;
    }
  };

  return { isDisabled, onClick };
};

export default useXendit;
