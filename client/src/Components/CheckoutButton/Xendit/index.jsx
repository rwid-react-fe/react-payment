import useXendit from "./useXendit";

const Xendit = () => {
  const {isDisabled, onClick} = useXendit();

  return (
    <button
      className="p-2 bg-blue-800 text-white rounded-md disabled:bg-gray-300 disabled:text-gray-400"
      onClick={onClick}
      disabled={isDisabled}
    >
      Xendit
    </button>
  );
};

export default Xendit;
