import useStripe from "./useStripe";

const Stripe = () => {
  const {isDisabled, onClick} = useStripe();

  return (
    <button
      className="p-2 bg-sky-600 text-white rounded-md disabled:bg-gray-300 disabled:text-gray-400"
      onClick={onClick}
      disabled={isDisabled}
    >
      Stripe
    </button>
  );
};

export default Stripe;
