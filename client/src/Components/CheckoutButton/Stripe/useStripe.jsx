import { useDispatch } from "react-redux";

import { useCart, clear } from "../../../store/cart/cartSlice";
/**
 * @param {RequestInfo | URL} endpoint
 * @param {RequestInit} init
 * @returns
 */
const makeRequest = async (endpoint, init) => {
  const response = await fetch(`http://localhost:3000/s${endpoint}`, {
    headers: {
      "Content-Type": "application/json",
    },
    ...init,
  });

  return await response.json();
};

const generateRequestBody = (cart) => {
  const line_items = cart.product.map(
    ({ name, price, qty: quantity, image }) => {
      return {
        price_data: {
          currency: "idr",
          unit_amount: price,
          product_data: {
            name,
            images: [image],
          },
        },
        quantity,
      };
    }
  );

  return {
    line_items,
    payment_method_types: ["card"],
    mode: "payment",
    success_url: "https://www.google.com",
  };
};

const useStripe = () => {
  const dispatch = useDispatch();

  const cart = useCart();

  const isDisabled = !cart.totalQty;

  const onClick = async () => {
    try {
      const request = generateRequestBody(cart);

      const sessions = await makeRequest("/sessions", {
        method: "POST",
        body: JSON.stringify(request),
      });

      window.open(sessions.url, "_blank");

      dispatch(clear());

      return;
    } catch (err) {
      console.log(err);
      throw err;
    }
  };

  return { isDisabled, onClick };
};

export default useStripe;
