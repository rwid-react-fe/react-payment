import { nanoid } from "nanoid";

import { clear, useCart } from "../../../store/cart/cartSlice";
import { useDispatch } from "react-redux";

const getMidtransToken = async (cart) => {
  const item_details = cart.product.map(({ name, price, qty: quantity }) => ({
    name,
    price,
    quantity,
  }));

  const body = {
    transaction_details: {
      order_id: `O-${nanoid(10)}`,
      gross_amount: cart.totalPrice,
    },

    item_details,
  };

  const response = await fetch("http://localhost:3000/m/snap", {
    headers: {
      "Content-Type": "application/json",
    },
    method: "post",
    body: JSON.stringify(body),
  });

  return await response.json();
};

// currying
const logger = (status, cb) => (result) => {
  if (status) console.log(status);

  if (result) console.log(result);

  if (!status)
    console.log("customer closed the popup without finishing the payment");

  if (cb instanceof Function) cb();
};

const MidtransSnap = () => {
  const cart = useCart();

  const dispatch = useDispatch();

  const clearCart = () => dispatch(clear());

  const isDisabled = !cart.totalQty;

  const onCheckout = async () => {
    const response = await getMidtransToken(cart);

    window.snap.pay(response.token, {
      onSuccess: logger("success", clearCart),
      onPending: logger("pending"),
      onError: logger("error"),
      onClose: logger(),
    });
  };

  return (
    <button
      className="p-2 bg-fuchsia-600 text-white rounded-md disabled:bg-gray-300 disabled:text-gray-400"
      onClick={onCheckout}
      disabled={isDisabled}
    >
      Midtrans Snap
    </button>
  );
};

export default MidtransSnap;
