import { useNavigate } from "react-router-dom";

import { useCart } from "../../../store/cart/cartSlice";

const MidtransCore = () => {
  const navigate = useNavigate();

  const cart = useCart();

  const isDisabled = !cart.totalQty;

  const onTransactionProcess = () => navigate("/checkout/m");

  return (
    <button
      className="p-2 bg-purple-600 text-white rounded-md disabled:bg-gray-300 disabled:text-gray-400"
      onClick={onTransactionProcess}
      disabled={isDisabled}
    >
      Midtrans Core
    </button>
  );
};

export default MidtransCore;
