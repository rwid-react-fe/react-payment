import "./ThreeDotSpinner.css";

const ThreeDotsSpinner = () => {
  return (
    <div className="h-screen flex items-center justify-center absolute inset-0 bg-royalBlue bg-opacity-25">
      <div className="three-body">
        <div className="three-body__dot" />
        <div className="three-body__dot" />
        <div className="three-body__dot" />
      </div>
    </div>
  );
};

export default ThreeDotsSpinner;
