import { formatPrice } from "../lib/format";

const ProductCartDetail = ({ product }) => {
  return (
    <div className="flex items-center gap-6">
      <img
        src={product.image}
        alt={product.name}
        className="w-24 h-24 object-contain border"
      />
      <div className="grid grid-cols-3 w-full items-center justify-between">
        <div className="space-y-2">
          <p className="max-w-64 font-bold">{product.name}</p>
          <p className="text-gray-500">{formatPrice(product.price)}</p>
        </div>
        <p> x {product.qty}</p>
        <p className="font-bold text-gray-500">{formatPrice(product.price * product.qty)}</p>
      </div>
    </div>
  );
};

export default ProductCartDetail;
