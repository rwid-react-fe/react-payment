/** @type {import("react").FC<{ wrapperProps: JSX.IntrinsicElements["div"], imgProps: JSX.IntrinsicElements["image"] }>} */
const PaymentChannel = (props) => {
  const { wrapperProps, imgProps } = props;

  return (
    <div
      {...wrapperProps}
      className="group border rounded-lg cursor-pointer shadow-md"
    >
      <img
        {...imgProps}
        className="w-20 h-20 object-contain mx-auto grayscale duration-100 group-hover:grayscale-0"
      />
    </div>
  );
};

export default PaymentChannel;
