import { CreditCardIcon } from "@heroicons/react/24/outline";

const CreditCardForm = ({ onCheckout }) => {
  return (
    <div className="max-w-md bg-white rounded-lg overflow-hidden shadow-lg p-6">
      <h2 className="text-lg font-semibold mb-4">Credit Card Information</h2>
      <div className="mb-4">
        <label
          htmlFor="cc-number"
          className="block text-sm font-medium text-gray-700 mb-1"
        >
          Credit Card Number:
        </label>
        <input
          type="text"
          id="cc-number"
          name="cc-number"
          placeholder="1234 5678 9012 3456"
          value="4811 1111 1111 1114"
          maxLength="19"
          required
          className="w-full rounded-md shadow-sm p-2"
          disabled
        />
      </div>
      <div className="grid grid-cols-2 gap-4 mb-4">
        <div>
          <label
            htmlFor="expiration-date"
            className="block text-sm font-medium text-gray-700 mb-1"
          >
            Expiration Date (MM/YY):
          </label>
          <input
            type="text"
            id="expiration-date"
            name="expiration-date"
            placeholder="MM/YYYY"
            value="02/2025"
            maxLength="5"
            required
            className="w-full rounded-md shadow-sm p-2"
            disabled
          />
        </div>
        <div>
          <label
            htmlFor="cvv"
            className="block text-sm font-medium text-gray-700 mb-1"
          >
            CVV:
          </label>
          <input
            type="text"
            id="cvv"
            name="cvv"
            placeholder="123"
            value="123"
            maxLength="4"
            required
            className="w-full rounded-md shadow-sm p-2"
            disabled
          />
        </div>
      </div>
      <div className="mb-4">
        <label
          htmlFor="name-on-card"
          className="block text-sm font-medium text-gray-700 mb-1"
        >
          Name on Card:
        </label>
        <input
          type="text"
          id="name-on-card"
          name="name-on-card"
          placeholder="John Doe"
          value="User Testing"
          required
          className="w-full rounded-md shadow-sm p-2"
          disabled
        />
      </div>

      <button className="bg-royalBlue p-2 rounded-md text-white flex items-center gap-4" onClick={onCheckout}>
        <span>Checkout</span>
        <CreditCardIcon className="w-6 h-6" />
      </button>
    </div>
  );
};

export default CreditCardForm;
