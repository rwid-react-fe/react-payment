// CartSummarySidebar.jsx
import { useDispatch } from "react-redux";
import { XMarkIcon } from "@heroicons/react/24/outline";

import { formatPrice } from "../lib/format";
import {
  addToCart,
  removeItemFromCart,
  useCart,
} from "../store/cart/cartSlice";
import { MidtransCore, MidtransSnap, Stripe, Xendit } from "./CheckoutButton";

const CartSummarySidebar = ({ closeSidebar, isSidebarOpen }) => {
  const dispatch = useDispatch();

  const cart = useCart();

  // Calculate total quantity and price
  const totalQuantity = cart.totalQuantity;
  const totalPrice = cart.totalPrice;

  const inc = (product) => dispatch(addToCart({ product }));
  const dec = (product) => dispatch(removeItemFromCart({ product }));

  return (
    <div
      className={`fixed inset-y-0 ${
        isSidebarOpen ? "left-0" : "-left-96"
      } bg-white w-96 p-4 shadow-md duration-200`}
    >
      <div className="flex justify-between items-center mb-4">
        <h2 className="text-2xl font-bold">Cart Summary</h2>
        <button onClick={closeSidebar} className="text-gray-500">
          <XMarkIcon className="w-6 h-6" />
        </button>
      </div>
      {cart.product.length ? (
        cart.product.map((item) => (
          <div key={item.id} className="grid grid-cols-6 items-center mb-2">
            <p className="col-span-2">{item.name}</p>
            <p className="col-span-3 text-center">
              {item.qty} x {formatPrice(item.price)}
            </p>

            <div className="flex items-center gap-1">
              <button
                className="bg-navyBlue text-white px-2 py-1"
                onClick={() => inc(item)}
              >
                +
              </button>
              <button
                className="border border-navyBlue px-2 py-1"
                onClick={() => dec(item)}
              >
                -
              </button>
            </div>
          </div>
        ))
      ) : (
        <p className="italic text-gray-500">Cart is empty.</p>
      )}
      <hr className="my-4" />
      <div className="flex justify-between items-center">
        <p className="font-semibold">Total Items:</p>
        <p>{totalQuantity}</p>
      </div>
      <div className="flex justify-between items-center">
        <p className="font-semibold">Total Price:</p>
        <p>{formatPrice(totalPrice)}</p>
      </div>
      <div className="grid grid-cols-3 mt-4 gap-2">
        <MidtransSnap />
        <MidtransCore />
        <Xendit />
        <Stripe />
      </div>
    </div>
  );
};

export default CartSummarySidebar;
