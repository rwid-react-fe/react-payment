// FloatingActionButton.jsx
import { ShoppingCartIcon } from "@heroicons/react/24/outline";
import CartSummarySidebar from "./CartSummarySidebar"; // Create CartSummarySidebar component
import { useCartSidebar } from "../context/CartSidebarContext";

const FloatingActionButton = () => {
  const { isSidebarOpen, toggleSidebar } = useCartSidebar();

  return (
    <div className="fixed bottom-8 right-8">
      <button
        onClick={toggleSidebar}
        className="bg-blue-500 text-white px-4 py-2 rounded-full flex items-center justify-center hover:bg-blue-600"
      >
        <ShoppingCartIcon className="h-6 w-6 mr-2" />
        Cart
      </button>
      <CartSummarySidebar
        closeSidebar={toggleSidebar}
        isSidebarOpen={isSidebarOpen}
      />
    </div>
  );
};

export default FloatingActionButton;
