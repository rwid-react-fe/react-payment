import React from "react";
import ReactDOM from "react-dom/client";
import { Provider } from "react-redux";

import App from "./App.jsx";
import "./index.css";

import store from "./store/index.js";
import CartSidebarProvider from "./context/CartSidebarContext.jsx";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <Provider store={store}>
      <CartSidebarProvider>
        <App />
      </CartSidebarProvider>
    </Provider>
  </React.StrictMode>
);
