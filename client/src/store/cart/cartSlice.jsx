import { createSlice } from "@reduxjs/toolkit";
import { useSelector } from "react-redux";

export const cartSlice = createSlice({
  name: "cart",
  initialState: {
    totalQty: 0,
    totalPrice: 0,
    product: [],
  },
  reducers: {
    addToCart: (state, action) => {
      const { product } = action.payload;

      const isItemExist = state.product.findIndex(
        (item) => item.id == product.id
      );

      if (isItemExist < 0) {
        state.product.push({ ...product, qty: 1 });
      }

      if (isItemExist > -1) {
        state.product[isItemExist].qty += 1;
      }

      state.totalQty = state.product.reduce((acc, curr) => acc + curr.qty, 0);

      state.totalPrice = state.product.reduce(
        (acc, curr) => acc + curr.qty * curr.price,
        0
      );

      return state;
    },
    removeItemFromCart: (state, action) => {
      const { product } = action.payload;

      const isItemExist = state.product.findIndex(
        (item) => item.id == product.id
      );

      if (isItemExist > -1) {
        const currProduct = state.product[isItemExist];

        if (currProduct.qty > 1) {
          state.product = state.product.map((_product) =>
            _product.id == currProduct.id
              ? { ..._product, qty: _product.qty - 1 }
              : _product
          );
        } else {
          state.product = state.product.filter(
            (_product) => _product.id != currProduct.id
          );
        }
      }

      state.totalQty = state.product.reduce((acc, curr) => acc + curr.qty, 0);

      state.totalPrice = state.product.reduce(
        (acc, curr) => acc + curr.qty * curr.price,
        0
      );

      return state;
    },
    clear: (state) => {
      state = {
        totalQty: 0,
        totalPrice: 0,
        product: [],
      };

      return state;
    },
  },
});

// Action creators are generated for each case reducer function
export const { addToCart, removeItemFromCart, clear } =
  cartSlice.actions;

export const useCart = () => useSelector((state) => state.cart);

export default cartSlice.reducer;
