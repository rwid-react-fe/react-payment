export const midtrans_channels = [
  {
    id: 4,
    name: "bca",
    src: "https://d2f3dnusg0rbp7.cloudfront.net/snap-preferences/sandbox/v1/logos/bca.svg",
    payment_type: "bank_transfer",
  },
  {
    id: 5,
    name: "mandiri",
    src: "https://d2f3dnusg0rbp7.cloudfront.net/snap-preferences/sandbox/v1/logos/mandiri.svg",
    payment_type: "bank_transfer",
  },
  {
    id: 6,
    name: "bni",
    src: "https://d2f3dnusg0rbp7.cloudfront.net/snap-preferences/sandbox/v1/logos/bni.svg",
    payment_type: "bank_transfer",
  },
  {
    id: 7,
    name: "bri",
    src: "https://d2f3dnusg0rbp7.cloudfront.net/snap-preferences/sandbox/v1/logos/bri.svg",
    payment_type: "bank_transfer",
  },
  {
    id: 8,
    name: "permata",
    src: "https://d2f3dnusg0rbp7.cloudfront.net/snap-preferences/sandbox/v1/logos/permata.svg",
    payment_type: "bank_transfer",
  },
  {
    id: 9,
    name: "visa",
    src: "https://d2f3dnusg0rbp7.cloudfront.net/snap-preferences/sandbox/v1/logos/visa.svg",
    payment_type: "credit_card",
  },
  {
    id: 10,
    name: "mastercard",
    src: "https://d2f3dnusg0rbp7.cloudfront.net/snap-preferences/sandbox/v1/logos/mastercard.svg",
    payment_type: "credit_card",
  },
  {
    id: 11,
    name: "jcb",
    src: "https://d2f3dnusg0rbp7.cloudfront.net/snap-preferences/sandbox/v1/logos/jcb.svg",
    payment_type: "credit_card",
  },
  {
    id: 12,
    name: "amex",
    src: "https://d2f3dnusg0rbp7.cloudfront.net/snap-preferences/sandbox/v1/logos/amex.svg",
    payment_type: "credit_card",
  },
  {
    id: 20,
    name: "alfamart",
    src: "https://d2f3dnusg0rbp7.cloudfront.net/snap-preferences/sandbox/v1/logos/alfamart.svg",
    payment_type: "cstore",
  },
  {
    id: 21,
    name: "alfamidi",
    src: "https://d2f3dnusg0rbp7.cloudfront.net/snap-preferences/sandbox/v1/logos/alfamidi.svg",
    payment_type: "cstore",
  },
  {
    id: 22,
    name: "dandan",
    src: "https://d2f3dnusg0rbp7.cloudfront.net/snap-preferences/sandbox/v1/logos/dandan.svg",
    payment_type: "cstore",
  },
  {
    id: 23,
    name: "indomaret",
    src: "https://d2f3dnusg0rbp7.cloudfront.net/snap-preferences/sandbox/v1/logos/indomaret.svg",
    payment_type: "cstore",
  },
];

export const xendit_channels = [
  {
    id: 1,
    name: "bri",
    src: "https://dashboard.xendit.co/assets/images/bri-logo.svg",
    payment_type: "bank_transfer",
  },
  {
    id: 2,
    name: "mandiri",
    src: "https://dashboard.xendit.co/assets/images/mandiri-logo.svg",
    payment_type: "bank_transfer",
  },
  {
    id: 3,
    name: "bni",
    src: "https://dashboard.xendit.co/assets/images/bni-logo.svg",
    payment_type: "bank_transfer",
  },
  {
    id: 4,
    name: "permata",
    src: "https://dashboard.xendit.co/assets/images/permata-logo.svg",
    payment_type: "bank_transfer",
  },
  {
    id: 5,
    name: "bca",
    src: "https://dashboard.xendit.co/assets/images/bca-logo.svg",
    payment_type: "bank_transfer",
  },
  {
    id: 6,
    name: "bss",
    src: "https://dashboard.xendit.co/assets/images/bss-logo.svg",
    payment_type: "bank_transfer",
  },
  {
    id: 7,
    name: "cimb",
    src: "https://dashboard.xendit.co/assets/images/cimb-logo.svg",
    payment_type: "bank_transfer",
  },
  {
    id: 8,
    name: "bjb",
    src: "https://dashboard.xendit.co/assets/images/bjb-logo.png",
    payment_type: "bank_transfer",
  },
  {
    id: 9,
    name: "bsi",
    src: "https://dashboard.xendit.co/assets/images/bsi-logo.png",
    payment_type: "bank_transfer",
  },
  {
    id: 10,
    name: "alfamart",
    src: "https://dashboard.xendit.co/assets/images/alfamart-logo.svg",
    payment_type: "cstore",
  },
  {
    id: 11,
    name: "indomaret",
    src: "https://dashboard.xendit.co/assets/images/indomaret-logo.svg",
    payment_type: "cstore",
  },
  {
    id: 12,
    name: "ovo",
    src: "https://dashboard.xendit.co/assets/images/ovo-logo.svg",
    payment_type: "ewallet"
  },
  {
    id: 13,
    name: "dana",
    src: "https://dashboard.xendit.co/assets/images/dana-logo.svg",
    payment_type: "ewallet"
  },
  {
    id: 14,
    name: "linkaja",
    src: "https://dashboard.xendit.co/assets/images/linkaja-logo.svg",
    payment_type: "ewallet"
  },
  {
    id: 15,
    name: "shopeepay",
    src: "https://dashboard.xendit.co/assets/images/shopeepay-logo.svg",
    payment_type: "ewallet"
  },
  {
    id: 16,
    name: "astrapay",
    src: "https://dashboard.xendit.co/assets/images/astrapay-logo.svg",
    payment_type: "ewallet"
  },
  {
    id: 17,
    name: "visa",
    src: "https://dashboard.xendit.co/assets/images/card-brands-logos/visa-settings-payment-methods.svg",
    payment_type: "credit_card",
  },
  {
    id: 18,
    name: "MASTERCARD",
    src: "https://dashboard.xendit.co/assets/images/card-brands-logos/MASTERCARD-settings.svg",
    payment_type: "credit_card",
  },
  {
    id: 19,
    name: "JCB",
    src: "https://dashboard.xendit.co/assets/images/card-brands-logos/JCB-settings.svg",
    payment_type: "credit_card",
  },
  {
    id: 20,
    name: "qris",
    src: "https://dashboard.xendit.co/assets/images/qris-logo.svg",
    payment_type: "qris"
  },
  {
    id: 21,
    name: "uangme",
    src: "https://dashboard.xendit.co/assets/images/uangme-logo.svg",
    payment_type: "ewallet"
  },
];
