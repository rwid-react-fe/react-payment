import { useState } from "react";

const useToggle = (bool = false) => {
    const [isOn, setIsOn] = useState(bool);

    const toggler = () => setIsOn(state => !state);

    return [isOn, toggler];
};

export default useToggle;