import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import toast, { Toaster } from "react-hot-toast";

import { useCart, clear } from "../../store/cart/cartSlice";
import useToggle from "../../hooks/useToggle";
import useMidtransCore from "./useMidtransCore";

import { ArrowLeftIcon } from "@heroicons/react/20/solid";

import ProductCartDetail from "../../Components/ProductCartDetail";

import { formatPrice } from "../../lib/format";
import { midtrans_channels } from "../../constant/payment_channels";
import CreditCardForm from "../../Components/CreditCardForm";
import Modal from "../../Components/Modal";
import ThreeDotsSpinner from "../../Components/ThreeDotsSpinner";
import PaymentChannel from "../../Components/PaymentChannel";

const card_channels = midtrans_channels.filter(
  (channel) => channel.payment_type == "credit_card"
);
const bank_channels = midtrans_channels.filter(
  (channel) => channel.payment_type == "bank_transfer"
);
const cstore_channels = midtrans_channels.filter(
  (channel) => channel.payment_type == "cstore"
);

const MidtransCheckout = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const cart = useCart();
  const { createCardRequest } = useMidtransCore();

  const [isModalOpen, toggleModal] = useToggle(false);
  const [isLoading, toggleLoading] = useToggle(false);

  const back = () => navigate(-1);

  const onCheckoutByCard = async () => {
    toggleLoading();
    toggleModal();

    try {
      const transaction = await createCardRequest();

      toast.success(transaction.status_message, { duration: 3000 });

      setTimeout(() => {
        toast.loading("Redirecting back", { duration: 2000 });
      }, 3000);
    } catch (err) {
      toast.error(`Pembayaran tidak berhasil!`);
    }

    toggleLoading();

    setTimeout(() => {
      dispatch(clear());
      navigate("/");
    }, 6000);
  };

  return (
    <>
      <div className="space-y-4 pb-10">
        <div className="bg-royalBlue p-4">
          <div className="container mx-auto">
            <p className="text-white text-4xl font-bold">Pembayaran</p>
          </div>
        </div>

        <div className="container mx-auto">
          <button className="flex items-center gap-2 duration-100 hover:underline">
            <ArrowLeftIcon className="w-6 h-6 " />
            <span onClick={back}>Kembali</span>
          </button>

          <div className="mt-9 space-y-6">
            <div className="space-y-6">
              <p className="text-xl font-bold">Detail Pembelian : </p>
              {cart.product.map((product) => (
                <ProductCartDetail key={product.id} product={product} />
              ))}

              <div className="flex items-center gap-12">
                <p className="text-lg font-bold">Total : </p>
                <p className="text-gray-500">{formatPrice(cart.totalPrice)}</p>
              </div>
            </div>

            <div className="space-y-4">
              <p className="text-xl font-bold">Pilih metode pembayaran</p>

              <div className="space-y-2">
                <p>Kartu Kredit : </p>
                <div className="grid grid-cols-6 gap-6">
                  {card_channels.map((channel) => {
                    return (
                      <PaymentChannel
                        key={channel.id}
                        wrapperProps={{ onClick: toggleModal }}
                        imgProps={{ src: channel.src, alt: channel.name }}
                      />
                    );
                  })}
                </div>
              </div>
              <div className="space-y-2">
                <p>Transfer Bank: </p>
                <div className="grid grid-cols-6 gap-6">
                  {bank_channels.map((channel) => {
                    return (
                      <PaymentChannel
                        key={channel.id}
                        imgProps={{ src: channel.src, alt: channel.name }}
                      />
                    );
                  })}
                </div>
              </div>
              <div className="space-y-2">
                <p>Toko Retail : </p>
                <div className="grid grid-cols-6 gap-6">
                  {cstore_channels.map((channel) => {
                    return (
                      <PaymentChannel
                        key={channel.id}
                        imgProps={{ src: channel.src, alt: channel.name }}
                      />
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Modal isOpen={isModalOpen} onClose={toggleModal}>
        <CreditCardForm onCheckout={onCheckoutByCard} />
      </Modal>

      {isLoading && <ThreeDotsSpinner />}

      <Toaster />
    </>
  );
};

export default MidtransCheckout;
