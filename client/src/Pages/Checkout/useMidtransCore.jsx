import { nanoid } from "nanoid";

import { formatCartItem } from "../../lib/format";
import { useCart } from "../../store/cart/cartSlice";

/**
 * @param {RequestInfo | URL} endpoint
 * @param {RequestInit} init
 * @returns
 */
const makeRequest = async (endpoint, init) => {
  const response = await fetch(`http://localhost:3000/m${endpoint}`, {
    headers: {
      "Content-Type": "application/json",
    },
    ...init,
  });

  return await response.json();
};

const generateRequestBody = (cart) => {
  const item_details = formatCartItem(cart);

  const body = {
    payment_type: null,
    transaction_details: {
      order_id: `O-${nanoid(10)}`,
      gross_amount: cart.totalPrice,
    },
    item_details,
  };

  return body;
};

const useMidtransCore = () => {
  const cart = useCart();

  const createCardRequest = async () => {
    try {
      const token = await makeRequest("/token");

      const request = {
        ...generateRequestBody(cart),
        payment_type: "credit_card",
        credit_card: {
          token_id: token.token_id,
        },
      };

      const transaction = await makeRequest("/charge", {
        method: "POST",
        body: JSON.stringify(request),
      });

      return transaction;
    } catch (err) {
      console.log(err);
      throw err;
    }
  };

  return {
    createCardRequest,
    makeRequest,
  };
};

export default useMidtransCore;
