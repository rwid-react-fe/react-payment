import { createContext, useContext, useState } from "react";

const CartSidebarContext = createContext();

export const useCartSidebar = () => useContext(CartSidebarContext);

const CartSidebarProvider = ({ children }) => {
  const [isSidebarOpen, setIsSidebarOpen] = useState(false);

  const toggleSidebar = () => setIsSidebarOpen((state) => !state);

  return (
    <CartSidebarContext.Provider value={{ isSidebarOpen, toggleSidebar }}>
      {children}
    </CartSidebarContext.Provider>
  );
};

export default CartSidebarProvider;
